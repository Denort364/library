@extends('layouts.app')
@section('title')Материалы@endsection
@section('content')

    <div class="container">
        <h1 class="my-md-5 my-4">Добавить материал</h1>
        <div class="row">
            <div class="col-lg-5 col-md-8">
                @include('inc.messages')
                <form action="{{ route('updateMaterialSubmit', $material->id) }}" method="post">
                    @csrf
                    <div class="form-floating mb-3">
                        <select class="form-select" name="floatingSelectType" id="floatingSelectType">
                            <option value="{{$material->type}}">{{$material->type}}</option>
                            @foreach($types as $type)
                                @if(!($type->name == $material->type))
                                    <option value="{{$type->name}}">{{$type->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        <label for="floatingSelectType">Тип</label>
                        <div class="invalid-feedback">
                            Пожалуйста, выберите значение
                        </div>
                    </div>
                    <div class="form-floating mb-3">
                        <select class="form-select" name="floatingSelectCategory" id="floatingSelectCategory">
                            <option value="{{$material->category}}">{{$material->category}}</option>
                            @foreach($categories as $category)
                                @if(!($category->name == $material->category))
                                    <option value="{{$category->name}}">{{$category->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        <label for="floatingSelectCategory">Категория</label>
                        <div class="invalid-feedback">
                            Пожалуйста, выберите значение
                        </div>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" value="{{$material->name}}" name="floatingName" class="form-control" placeholder="Напишите название" id="floatingName">
                        <label for="floatingName">Название</label>
                        <div class="invalid-feedback">
                            Пожалуйста, заполните поле
                        </div>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" value="{{$material->author}}" name="floatingAuthor" class="form-control" placeholder="Напишите авторов" id="floatingAuthor">
                        <label for="floatingAuthor">Авторы</label>
                        <div class="invalid-feedback">
                            Пожалуйста, заполните поле
                        </div>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" name="floatingDescription" placeholder="Напишите краткое описание" id="floatingDescription"
                                  style="height: 100px">{{$material->description}}</textarea>
                        <label for="floatingDescription">Описание</label>
                        <div class="invalid-feedback">
                            Пожалуйста, заполните поле
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Обновить</button>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
