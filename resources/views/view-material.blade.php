@extends('layouts.app')
@section('title')Материалы@endsection
@section('content')


            <div class="container">
                <h1 class="my-md-5 my-4">{{$material->name}}</h1>
                <div class="row mb-3">
                    <div class="col-lg-6 col-md-8">
                        <div class="d-flex text-break">
                            <p class="col fw-bold mw-25 mw-sm-30 me-2">Авторы</p>
                            <p class="col">{{$material->author}}</p>
                        </div>
                        <div class="d-flex text-break">
                            <p class="col fw-bold mw-25 mw-sm-30 me-2">Тип</p>
                            <p class="col">{{$material->type}}</p>
                        </div>
                        <div class="d-flex text-break">
                            <p class="col fw-bold mw-25 mw-sm-30 me-2">Категория</p>
                            <p class="col">{{$material->category}}</p>
                        </div>
                        <div class="d-flex text-break">
                            <p class="col fw-bold mw-25 mw-sm-30 me-2">Описание</p>
                            <p class="col">{{$material->description}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <form action="{{ route('addTag', $material->id) }}" method="post">
                            @csrf
                            <h3>Теги</h3>
                            <div class="input-group mb-3">
                                <select class="form-select" name="floatingName" id="selectAddTag" aria-label="Добавьте автора">
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->name}}">{{$tag->name}}</option>
                                    @endforeach

                                </select>
                                <button class="btn btn-primary" type="submit">Добавить</button>
                            </div>
                        </form>
                        <ul class="list-group mb-4">

                            @if(!($material->tags == null))
                            @foreach( json_decode($material->tags) as $tag)
                            <li class="list-group-item list-group-item-action d-flex justify-content-between">
                                <a href="#" class="me-3">
                                    {{$tag}}
                                </a>
                                <a href="{{ route('deleteMaterialTag', [$material->id, $tag]) }}" class="text-decoration-none">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         class="bi bi-trash" viewBox="0 0 16 16">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                        <path fill-rule="evenodd"
                                              d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                    </svg>
                                </a></li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class="col-md-6">
                        @include('inc.modal-window')
                        <div class="d-flex justify-content-between mb-3">
                            <h3>Ссылки</h3>
                            <a class="btn btn-primary" data-bs-toggle="modal"  data-bs-target="#exampleModal" href="#exampleModalToggle" role="button">Добавить</a>
                        </div>
                        <ul class="list-group mb-4">
                            @if(!($material->links == null))
                            @foreach( json_decode($material->links) as $link)
                            <li class="list-group-item list-group-item-action d-flex justify-content-between">
                                <a href="https://{{$link}}" class="me-3">
                                    {{$link}}
                                </a>
                                <span class="text-nowrap">
                            <a href="{{ route('deleteLink', [$material->id, $link]) }}" class="text-decoration-none">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-trash" viewBox="0 0 16 16">
                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                    <path fill-rule="evenodd"
                                          d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                </svg>
                            </a>
                            </span>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
@endsection
