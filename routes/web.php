<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MaterialsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TagController;

Route::get('/', [MaterialsController::class,'allData']
)->name('materials');

Route::get('/create-material', [MaterialsController::class,'creatMaterial'])->name('creatMaterial');

Route::get('/create-material/{id}', [MaterialsController::class,'materialUpdate'])->name('materialUpdate');

Route::get('/create-category/{id}', [CategoryController::class,'createCategory'])->name('createCategory');

Route::get('/create-tag/{id}', [TagController::class,'createTag'])->name('createTag');

Route::get('/list-category', [CategoryController::class,'categoryAllData'])->name('category');

Route::get('/list-tag/', [TagController::class,'tagAllData'])->name('tag');

Route::get('/view-material/{id}', [MaterialsController::class,'oneData'])->name('viewMaterial');

Route::post('/create-material', [MaterialsController::class,'submit'])->name('materialSubmit');

Route::post('/view-materialsLink/{id}', [MaterialsController::class,'addLink'])->name('links');

Route::get('/view-materialsLink/{id}/{link}', [MaterialsController::class,'deleteLink'])->name('deleteLink');

Route::post('/create-material/{id}', [MaterialsController::class,'updateSubmit'])->name('updateMaterialSubmit');

Route::get('/{id}', [MaterialsController::class,'materialDelete'])->name('materialDelete');

Route::post('/create-category/{id}', [CategoryController::class,'categorySubmit'])->name('categorySubmit');

Route::get('/list-category/{id}', [CategoryController::class,'deleteCategory'])->name('deleteCategory');

Route::post('/create-tag/{id}', [TagController::class,'tagSubmit'])->name('tagSubmit');

Route::get('/list-tag/{id}', [TagController::class,'deleteTag'])->name('deleteTag');

Route::post('/view-materialTag/{id}', [MaterialsController::class,'addTag'])->name('addTag');

Route::get('/view-materialTag/{id}/{tag}', [MaterialsController::class,'deleteMaterialTag'])->name('deleteMaterialTag');

Route::post('/search', [MaterialsController::class,'search'])->name('search');



