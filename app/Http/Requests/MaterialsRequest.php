<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaterialsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'floatingSelectType' => 'required',
            'floatingSelectCategory' => 'required',
            'floatingName' => 'required',
        ];
    }
    public  function  messages()
    {
        return [
            'floatingSelectType.required' => 'Поле тип является обязательным',
            'floatingSelectCategory.required' => 'Поле категория является обязательным',
            'floatingName.required' => 'Поле название является обязательным'
        ];
    }
}
