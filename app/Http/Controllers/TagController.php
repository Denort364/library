<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Models\Tag;

class TagController extends Controller
{
    public function tagSubmit($id, TagRequest $req)
    {
        if($id == 0){
            $tag = new Tag();
            $tag->name = $req->input("floatingName");
            $tag->save();
        } else {
            $tag = Tag::find($id);
            $tag->name = $req->input("floatingName");
            $tag->save();
        }
        return redirect()->route('tag')->with('success', 'Тег успешно загружен');
    }
    public function tagAllData()
    {
        return view('list-tag', ['tags' => Tag::all()]);
    }
    public function createTag($id)
    {
        return view('create-tag', ['id' => $id]);
    }
    public function deleteTag($id)
    {
        Tag::find($id)->delete();
        return redirect()->route('tag')->with('success', 'Тег успешно удален');
    }

}
