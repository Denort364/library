<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MaterialsRequest;
use App\Http\Requests\LinkRequest;
use App\Http\Requests\TagRequest;
use App\Models\Materials;
use App\Models\Category;
use App\Models\Type;
use App\Models\Tag;
use App\Models\materialSearch;

class MaterialsController extends Controller
{
    public function submit(MaterialsRequest $req)
    {
        $materials = new Materials();
        $materials->type = $req->input("floatingSelectType");
        $materials->category = $req->input("floatingSelectCategory");
        $materials->name = $req->input("floatingName");
        $materials->author = $req->input("floatingAuthor");
        $materials->description = $req->input("floatingDescription");
        $materials->save();
        return redirect()->route('materials')->with('success', 'Материал успешно загружен');
    }

    public function allData()
    {
        return view('list-materials', ['materials' => Materials::all(), 'search' => null]);
    }
    public function oneData($id)
    {
        return view('view-material', ['material' => Materials::find($id), 'tags' => Tag::all()]);
    }
    public function addLink($id ,LinkRequest $req)
    {
        $materials = Materials::find($id);
        if(!($materials->links == null)){
            $materials->links = json_encode(array_merge(json_decode($materials->links), [$req->input('link')]));
        }else{
            $materials->links = json_encode([$req->input('link')]);
        }

        $materials->save();
        return redirect()->route('viewMaterial', $id);
    }
    public function deleteLink($id, $link)
    {
        $material = Materials::find($id);

        $links = json_decode($material->links);
        unset($links[array_search($link, $links)]);
        $material->links = json_encode(array_merge($links));
        $material->save();
        return redirect()->route('viewMaterial', $id);
    }
    public function materialUpdate($id)
    {
        return view('update-material', ['material' => Materials::find($id), 'types' => Type::all(), 'categories'=> Category::all()]);
    }
    public function creatMaterial()
    {
        return view('create-material', ['types' => Type::all(), 'categories'=> Category::all()]);
    }
    public function updateSubmit($id , MaterialsRequest $req)
    {
        $materials = $materials = Materials::find($id);
        $materials->type = $req->input("floatingSelectType");
        $materials->category = $req->input("floatingSelectCategory");
        $materials->name = $req->input("floatingName");
        $materials->author = $req->input("floatingAuthor");
        $materials->description = $req->input("floatingDescription");
        $materials->save();
        return redirect()->route('materials')->with('success', 'Материал успешно обновлен');
    }
    public function materialDelete($id)
    {
        Materials::find($id)->delete();
        return redirect()->route('materials')->with('success', 'Материал успешно удален');
    }
    public function addTag($id ,TagRequest $req)
    {
        $materials = Materials::find($id);
        if(!($materials->tags == null)){
            $materials->tags = json_encode(array_merge(json_decode($materials->tags), [$req->input('floatingName')]));
        }else{
            $materials->tags = json_encode([$req->input('floatingName')]);
        }

        $materials->save();
        return redirect()->route('viewMaterial', $id);
    }
    public function deleteMaterialTag($id, $tag)
    {
        $material = Materials::find($id);

        $tags = json_decode($material->tags);
        unset($tags[array_search($tag, $tags)]);
        $material->tags = json_encode(array_merge($tags));
        $material->save();
        return redirect()->route('viewMaterial', $id);
    }
    public function search(Request $req)
    {
        $materials = Materials::all();


        return view('list-materials', ['materials' => Materials::all(), 'search' => $req->input('search')]);
    }

}

