<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    public function categorySubmit($id, CategoryRequest $req)
    {
        if($id == 0){
            $category = new Category();
            $category->name = $req->input("floatingName");
            $category->save();
        } else {
            $category = Category::find($id);
            $category->name = $req->input("floatingName");
            $category->save();
        }
        return redirect()->route('category')->with('success', 'Категория успешно загружена');
    }
    public function categoryAllData()
    {
        return view('list-category', ['category' => Category::all()]);
    }

    public function createCategory($id)
    {
        return view('create-category', ['id' => $id]);
    }
    public function deleteCategory($id)
    {
        Category::find($id)->delete();
        return redirect()->route('category')->with('success', 'Категория успешно удалена');
    }


}
